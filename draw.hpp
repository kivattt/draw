#ifndef DRAW_HPP
#define DRAW_HPP

#include <iostream>
#include <SFML/Graphics.hpp>
#include <thread>
#include <string>
#include <vector>

#include "sfmlui/sfmlui.hpp"

using std::string;
using std::vector;

namespace draw{
	unsigned long long get_approx_bytes_of_img_in_memory(const sf::Image img){
		return img.getSize().x * img.getSize().y * 4;
	}

	class Draw{
		public:

		int run(const string initialFilename, const string initialConfigFilename){
			sf::Image image;
			if (!image.loadFromFile(initialFilename)){
				std::cerr << "Could not open image file " << initialFilename << '\n';
				return 3;
			}

			const sf::Vector2u imageSize = image.getSize();
			std::cout << "Opened " << initialFilename << "\n";
			std::cout << "Information:\n";
			std::cout << "\tWidth: " << imageSize.x << " Height: " << imageSize.y << "\n";
			std::cout << '\t' << imageSize.x*imageSize.y << " pixels\n";
			std::cout << "\tApproximately " << get_approx_bytes_of_img_in_memory(image) / 1000.0 << "kb in memory\n";

			std::cout << "Copying image into texture...";
			sf::Texture imageTexture;
			imageTexture.loadFromImage(image);
			std::cout << " Done\n";

			draw::Config config = draw::read_config(initialConfigFilename);

			std::cout << "Default zoom level:   " << config.defaultzoomlevel << '\n';
			std::cout << "Zoom sensitivity:     " << config.zoomsensitivity << '\n';
			std::cout << "Rotation sensitivity: " << config.rotationsensitivity << '\n';
			std::cout << "Opening window with size " << config.resolution.x << 'x' << config.resolution.y << ", framerate " << config.framerate << '\n';

			sf::RenderWindow window(sf::VideoMode(config.resolution.x, config.resolution.y), "draw - " + initialFilename, sf::Style::Titlebar | sf::Style::Close);
			window.setFramerateLimit(config.framerate);

			sf::View view;
			view.reset(sf::FloatRect(0,0, window.getSize().x, window.getSize().y));
			view.setViewport(sf::FloatRect(0,0, 1,1));
			view.setCenter(imageSize.x/2, imageSize.y/2);
			view.zoom(1 / config.defaultzoomlevel);
			window.setView(view);

			sf::Sprite imageSpr;

			while (window.isOpen()){
				sf::Event e;
				while (window.pollEvent(e)){
					if (e.type == sf::Event::Closed)
						window.close();

					if (e.type == sf::Event::KeyPressed){
						std::cout << "Keycode: " << e.key.code << '\n';
						if (e.key.code == sf::Keyboard::Up){
							view.move(0, -config.moveupsensitivity);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Down){
							view.move(0, config.movedownsensitivity);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Left){
							view.move(-config.moveleftsensitivity, 0);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Right){
							view.move(config.moveleftsensitivity, 0);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Add){
							view.zoom(1 / config.zoomsensitivity);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Subtract){
							view.zoom(config.zoomsensitivity);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Comma){
							view.rotate(config.rotationsensitivity);
							window.setView(view);
						} else if (e.key.code == sf::Keyboard::Period){
							view.rotate(-config.rotationsensitivity);
							window.setView(view);
						}
					}
				}

				window.clear();

				auto mousePos = sf::Mouse::getPosition(window);
				auto worldPos = window.mapPixelToCoords(mousePos);

				imageSpr.setTexture(imageTexture);
				window.draw(imageSpr);

				window.display(); 	
			}

			return 0;
		}
	};
}

#endif // DRAW_HPP
