#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <cstdlib>

#include "configreader.hpp"
#include "draw.hpp"

using std::string;
using std::vector;

void usage(){
	std::cout << "Usage: draw [image file] [config file (default ../config)]\n";
}

int main(int argc, char *argv[]){
	if (argc < 2){
		usage();
		return 1;
	}

	const string filename = argv[1];
	if (filename.empty()){
		usage();
		return 2;
	}

	string configFilename = "../config";

	if (argc >= 3)
		configFilename = argv[2];

	if (configFilename.empty()){
		usage();
		return 3;
	}
		

	draw::Draw program;
	return program.run(filename, configFilename);
}
