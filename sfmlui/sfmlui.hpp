/* sfmlui.hpp
 * kivattt 2019
 * gitlab.com/kivattt/sfmlui
 * sfmlui - User interface elements for SFML
*/

#ifndef SFMLUI_HPP
#define SFMLUI_HPP

#include <SFML/Graphics.hpp>
#include <string>

#include "sfmlextensionminimal.hpp"

using std::string;

namespace SFMLUI{
	sf::Color blankColor{17,19,20}, textColor{255,255,255}, backgroundColor{40,48,50}, activeColor{30,34,48}, inactiveColor{55,66,100};
	const unsigned outlineSize=2;

	class Element{
		public:

		virtual void set_font(sf::Font &font, const unsigned charSize=14){}
		virtual void set_size(const float w, const float h){}
		virtual void set_position(const float x, const float y){}
		virtual void draw(sf::RenderWindow &window){}
		virtual void update(sf::RenderWindow &window, const sf::Event e = sf::Event()){}

		virtual sf::Vector2f get_size(){return {};}
		virtual sf::Vector2f get_position(){return {};}

		// Lowest/rightmost positions where all of the element would still be visible in the window
		virtual float get_leftmost_position(sf::RenderWindow &window){return 0.0f;}
		virtual float get_rightmost_position(sf::RenderWindow &window){return 0.0f;}
		virtual float get_highest_position(sf::RenderWindow &window){return 0.0f;}
		virtual float get_lowest_position(sf::RenderWindow &window){return 0.0f;}
	};

	class Button : public Element{
		protected:
		sf::Text text;
		sf::RectangleShape outlineRect;
		SFMLExtensionMinimal sfmlExtensionMinimal;
		const unsigned boundarySize=6;

		public:
		bool active=false;

		Button(){
			outlineRect.setOutlineThickness(outlineSize);
			text.setFillColor(textColor);
			outlineRect.setOutlineColor(inactiveColor);
			outlineRect.setFillColor(backgroundColor);
		}

		void set_font(sf::Font &font, const unsigned charSize=14){text.setFont(font);text.setCharacterSize(charSize);}
		void set_text(const string newText){text.setString(newText);}
		void set_size(const float w, const float h){outlineRect.setSize({w,h});}
		void set_position(const float x, const float y){
			outlineRect.setPosition(x,y);
			// TODO Set the texts y position so that the text is centered by height in the outline rect
			// And change boundarySize with it
			text.setPosition(x + boundarySize, y + boundarySize);
		}
		sf::Vector2f get_size(){return outlineRect.getSize();}
		sf::Vector2f get_position(){return outlineRect.getPosition();}
		float get_leftmost_position(sf::RenderWindow &window){return outlineRect.getOutlineThickness();}
		float get_rightmost_position(sf::RenderWindow &window){return window.getSize().x - outlineRect.getSize().x - outlineRect.getOutlineThickness();}
		float get_highest_position(sf::RenderWindow &window){return outlineRect.getOutlineThickness();}
		float get_lowest_position(sf::RenderWindow &window){return window.getSize().y - outlineRect.getSize().y - outlineRect.getOutlineThickness();}
		string get_text(){return text.getString();}

		void draw(sf::RenderWindow &window){
			outlineRect.setOutlineColor(active ? activeColor : inactiveColor);

			window.draw(outlineRect);
			window.draw(text);
		}

		virtual void update(sf::RenderWindow &window){
			const sf::Vector2i mousePos = sf::Mouse::getPosition(window);
			active = sfmlExtensionMinimal.m1_pressed() && outlineRect.getGlobalBounds().contains(mousePos.x, mousePos.y);
		}
	};

	class TogglingButton : public Button{
		public:
		bool toggled=false;

		void update(sf::RenderWindow &window){
			const sf::Vector2i mousePos = sf::Mouse::getPosition(window);
			if (sfmlExtensionMinimal.m1_pressed() && outlineRect.getGlobalBounds().contains(mousePos.x, mousePos.y)){
				active^=1;
				toggled=true;
			} else{
				toggled=false;
			}
		}
	};

	class Label : public Element{
		protected:
		sf::Text text;
		sf::RectangleShape backgroundRect;
		const unsigned boundarySize=6;

		public:

		Label(){
			text.setFillColor(textColor);
			backgroundRect.setFillColor(backgroundColor);
		}

		void set_font(sf::Font &font, const unsigned charSize=14){text.setFont(font);text.setCharacterSize(charSize);}
		void set_text(const string newText){text.setString(newText);}
		void set_size(const float w, const float h){backgroundRect.setSize({w,h});}
		void set_position(const float x, const float y){backgroundRect.setPosition(x,y);text.setPosition(x + boundarySize, y + boundarySize);}
		sf::Vector2f get_size(){return backgroundRect.getSize();}
		sf::Vector2f get_position(){return backgroundRect.getPosition();}
		float get_rightmost_position(sf::RenderWindow &window){return window.getSize().x - backgroundRect.getSize().x;}
		float get_lowest_position(sf::RenderWindow &window){return window.getSize().y - backgroundRect.getSize().y;}
		string get_text(){return text.getString();}

		void draw(sf::RenderWindow &window){
			window.draw(backgroundRect);
			window.draw(text);
		}
	};

	class VerticalScrollBar : public Element{
		protected:
		sf::RectangleShape outlineRect;
		sf::RectangleShape positionIndicatorRect;
		double scrollPosition=0; // Position from 0 (min) to 1 (max)

		template <class A, class B, class C>
		A restrict_to_number_range(const A n, const B low, const C high){
			if (n<low)
				return low;
			if (n>high)
				return high;
			return n;
		}

		public:

		VerticalScrollBar(){
			outlineRect.setOutlineThickness(outlineSize);
			outlineRect.setOutlineColor(inactiveColor);
			outlineRect.setFillColor(backgroundColor);
			positionIndicatorRect.setFillColor(inactiveColor);
			positionIndicatorRect.setSize({outlineRect.getSize().x, 20}); // 20 is the default positionIndicator size
		}

		void set_size(const float w, const float h){outlineRect.setSize({w,h}); positionIndicatorRect.setSize({w, positionIndicatorRect.getSize().y});}
		void set_position_indicator_size(const float size){positionIndicatorRect.setSize({positionIndicatorRect.getSize().x, size});}
		void set_position(const float x, const float y){
			outlineRect.setPosition(x,y);
			const float positionIndicatorLowestOnScreenPos = outlineRect.getPosition().y + outlineRect.getSize().y - positionIndicatorRect.getSize().y;
			positionIndicatorRect.setPosition(x, map_number_ranges(scrollPosition, 0, 1, outlineRect.getPosition().y, positionIndicatorLowestOnScreenPos));
		}
		sf::Vector2f get_size(){return outlineRect.getSize();}
		sf::Vector2f get_position(){return outlineRect.getPosition();}
		float get_leftmost_position(){return outlineRect.getOutlineThickness();}
		float get_rightmost_position(sf::RenderWindow &window){return window.getSize().x - outlineRect.getSize().x - outlineRect.getOutlineThickness();}
		float get_highest_position(){return outlineRect.getOutlineThickness();}
		float get_lowest_position(sf::RenderWindow &window){return window.getSize().y - outlineRect.getSize().y - outlineRect.getOutlineThickness();}
		double get_scroll_position(){return scrollPosition;}

		// Copied from arduinos map() function, changed to use double (https://www.arduino.cc/reference/en/language/functions/math/map/)
		double map_number_ranges(double n, double in_min, double in_max, double out_min, double out_max){
			return (n - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
		}

		void update(sf::RenderWindow &window){
			const sf::Vector2i mousePos = sf::Mouse::getPosition(window);
			if(sf::Mouse::isButtonPressed(sf::Mouse::Button::Left) && outlineRect.getGlobalBounds().contains(mousePos.x, mousePos.y)){
				outlineRect.setOutlineColor(activeColor);

				const float positionIndicatorLowestOnScreenPos = outlineRect.getPosition().y + outlineRect.getSize().y - positionIndicatorRect.getSize().y;

				positionIndicatorRect.setPosition(positionIndicatorRect.getPosition().x, restrict_to_number_range(mousePos.y - positionIndicatorRect.getSize().y/2, outlineRect.getPosition().y, positionIndicatorLowestOnScreenPos));

				scrollPosition = map_number_ranges(positionIndicatorRect.getPosition().y, outlineRect.getPosition().y, positionIndicatorLowestOnScreenPos, 0, 1);
			} else{
				outlineRect.setOutlineColor(inactiveColor);
			}
		}

		void draw(sf::RenderWindow &window){
			window.draw(outlineRect);
			window.draw(positionIndicatorRect);
		}
	};

	// TODO Create TextBox (this is why Element has a sf::Event argument in update())
	// TODO Create HorizontalScrollBar
};

#endif // SFMLUI_HPP
