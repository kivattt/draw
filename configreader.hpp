#ifndef CONFIGREADER_HPP
#define CONFIGREADER_HPP

#include <iostream>
#include <SFML/Graphics.hpp>
#include <string>
#include <fstream>
#include <vector>
#include <algorithm>

using std::string;
using std::vector;

namespace draw{
	struct Config{
		sf::Vector2u resolution{1280,720};
		unsigned framerate=60;
		double defaultzoomlevel=1;
		double zoomsensitivity=0.1;
		double rotationsensitivity=0.0;
		bool debug=false;

		double moveupsensitivity=10.0;
		double movedownsensitivity=10.0;
		double moveleftsensitivity=10.0;
		double moverightsensitivity=10.0;
	};

	vector <string> split_str(const string str, const string delimiters){
		vector <string> out;

		string buffer;
		for (char chr : str){
			// Character is a delimiter
			if (std::find(delimiters.begin(),delimiters.end(), chr) != delimiters.end()){
				if (buffer.empty()) continue;

				out.push_back(buffer);
				buffer.clear();
				continue;
			}
			buffer += chr;
		}

		if (!buffer.empty())
			out.push_back(buffer);

		return out;
	}

	Config read_config(const string filename){
		Config out;
		std::ifstream configFile(filename.c_str());
		if (!configFile.is_open()){
			std::cerr << "Could not open config file " << filename << '\n';
			return out;
		}

		std::cout << "Opened config file " << filename << '\n';

		string line;
		for (unsigned long long lineNum=0; std::getline(configFile, line); ++lineNum){
			if (line[0] == '#' || line.empty()) continue;

			vector <string> splitLine = split_str(line, " \t");
			if (splitLine.empty()) continue;


			if (splitLine[0] == "resolution"){
				if (splitLine.size() != 3) continue;
					const sf::Vector2u oldResolution = out.resolution;
					try{
						out.resolution.x = std::stoi(splitLine[1]);
						out.resolution.y = std::stoi(splitLine[2]);
					} catch (std::invalid_argument &){
						std::cerr << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
						out.resolution = oldResolution;
						
					}
			} else if (splitLine[0] == "framerate"){
				if (splitLine.size() != 2) continue;

				const unsigned oldFramerate = out.framerate;
				try{
					out.framerate = std::stoi(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cerr << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
					out.framerate = oldFramerate;
				}
			} else if (splitLine[0] == "defaultzoomlevel"){
				if (splitLine.size() != 2) continue;

				const double oldDefaultZoomLevel = out.defaultzoomlevel;
				try{
					out.defaultzoomlevel = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cerr << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
					out.defaultzoomlevel = oldDefaultZoomLevel;
				}
			} else if (splitLine[0] == "zoomsensitivity"){
				if (splitLine.size() != 2) continue;

				const double oldZoomSensitivity = out.zoomsensitivity;
				try{
					out.zoomsensitivity = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cerr << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
					out.zoomsensitivity = oldZoomSensitivity;
				}
			} else if (splitLine[0] == "debug"){
				if (splitLine.size() != 2) continue;

				out.debug = splitLine[1] == "true";

				if (splitLine[1] == "true")
					out.debug = true;
				else if (splitLine[1] == "false")
					out.debug = false;
				else
					std::cout << "Invalid config line " << lineNum << ": Value is not true/false, ignoring\n";
			} else if (splitLine[0] == "rotationsensitivity"){
				const double oldRotationSensitivity = out.rotationsensitivity;
				try{
					out.rotationsensitivity = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
//					out.rotationsensitivity = oldRotationSensitivity;
					std::cout << "Rotation senssss::: " << out.rotationsensitivity << '\n';
				}
			} else if (splitLine[0] == "moveupsensitivity"){
				try{
					out.moveupsensitivity = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cout << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
				}
			} else if (splitLine[0] == "movedownsensitivity"){
				try{
					out.movedownsensitivity = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cout << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
				}
			} else if (splitLine[0] == "moveleftsensitivity"){
				try{
					out.moveleftsensitivity = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cout << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
				}
			} else if (splitLine[0] == "moverightsensitivity"){
				try{
					out.moverightsensitivity = std::stod(splitLine[1]);
				} catch (std::invalid_argument &){
					std::cout << "Invalid config line " << lineNum << ": Value is not a number, ignoring\n";
				}
			} else{
				std::cerr << "Invalid config line " << lineNum << ": Invalid value name, ignoring\n";
			}
		}
		return out;
	}
}

#endif // CONFIGREADER_HPP
